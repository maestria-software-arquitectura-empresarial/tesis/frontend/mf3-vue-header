# Utilizamos una imagen de node como base
FROM node:latest

# Establecemos el directorio de trabajo en el contenedor
WORKDIR /app

# Copiamos los archivos de la aplicación al directorio de trabajo
COPY ./headervue/package.json ./headervue/package-lock.json ./

# Instalamos las dependencias del proyecto
RUN npm install

# Copiamos el resto de los archivos de la aplicación al directorio de trabajo
COPY ./headervue/ ./

# Exponemos el puerto 9454 (el puerto por defecto de la aplicación React)
EXPOSE 9454

# Comando para iniciar la aplicación cuando se ejecute el contenedor
CMD ["npm", "run", "serve"]


# docker build -t headervue_imagen .
# docker stop headervue_contenedor
# docker rm headervue_contenedor
# docker run -p 9454:9454 --name headervue_contenedor headervue_imagen

# mf1-react-carga
# body1 npm run build